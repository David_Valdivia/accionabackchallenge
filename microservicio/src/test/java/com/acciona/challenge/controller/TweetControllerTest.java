package com.acciona.challenge.controller;

import com.acciona.challenge.domain.entity.Tweet;
import com.acciona.challenge.service.TweetService;
import com.google.gson.Gson;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.mockito.BDDMockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import static org.junit.jupiter.api.Assertions.*;
import java.util.ArrayList;
import java.util.List;

@AutoConfigureMockMvc
@SpringBootTest
public class TweetControllerTest {

    @MockBean
    private TweetService tweetService;

    @Autowired
    private TweetController tweetController;

    @Autowired
    private MockMvc mockMvc;

    private static final String URL = "http://localhost:8080/tweets";

    @Before
    public void setUp(){
        tweetController = new TweetControllerImpl(tweetService);
        mockMvc = MockMvcBuilders.standaloneSetup(tweetController).build();
    }

    @Test
    void getAllTweets() throws Exception {
        List<Tweet> tweetList = new ArrayList<>();
        tweetList.add(new Tweet("Prueba","Prueba","Prueba","Prueba",false, null));
        tweetList.add(new Tweet("Prueba2","Prueba2","Prueba2","Prueba2",false, null));
        BDDMockito.given(tweetService.getAllTweets()).willReturn(tweetList);
        Gson gson = new Gson();
        JSONArray myjson = new JSONArray(gson.toJson(tweetList));
        RequestBuilder request = MockMvcRequestBuilders.get(URL).contentType(MediaType.APPLICATION_JSON).content(String.valueOf(myjson));
        MvcResult result = mockMvc.perform(request).andReturn();
        assertTrue(result.getResponse().getContentAsString().contains("\"_id\":\"Prueba\",\"user\":\"Prueba\",\"text\":\"Prueba\",\"location\":\"Prueba\",\"validation\":false}"));
    }

    @Test
    void validateTweet() throws Exception {
        Tweet tweet = new Tweet("Prueba","Prueba","Prueba","Prueba",false, null);
        BDDMockito.given(tweetService.validateTweet(tweet.get_id())).willReturn(true);
        Gson gson = new Gson();
        JSONObject myjson = new JSONObject(gson.toJson(tweet));
        RequestBuilder request = MockMvcRequestBuilders.put(URL+"/"+tweet.get_id()+"/validate").contentType(MediaType.APPLICATION_JSON).content(String.valueOf(myjson));
        MvcResult result = mockMvc.perform(request).andReturn();
        assertTrue(result.getResponse().getContentAsString().contains("true"));
    }

    @Test
    void getValidatedTweets() throws Exception {
        List<Tweet> tweetList = new ArrayList<>();
        Tweet tweet = new Tweet("Prueba","Prueba","Prueba","Prueba",true, null);
        tweetList.add(tweet);
        BDDMockito.given(tweetService.getValidatedTweets(tweet.getUser())).willReturn(tweetList);
        Gson gson = new Gson();
        JSONArray myjson = new JSONArray(gson.toJson(tweetList));
        RequestBuilder request = MockMvcRequestBuilders.get(URL+"/users/"+tweet.getUser()+"/validated-tweets").contentType(MediaType.APPLICATION_JSON).content(String.valueOf(myjson));
        MvcResult result = mockMvc.perform(request).andReturn();
        assertTrue(result.getResponse().getContentAsString().contains("\"_id\":\"Prueba\",\"user\":\"Prueba\",\"text\":\"Prueba\",\"location\":\"Prueba\",\"validation\":true}"));
    }

}
