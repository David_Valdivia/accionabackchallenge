package com.acciona.challenge.controller;

import com.acciona.challenge.domain.entity.Hashtag;
import com.acciona.challenge.service.HashtagService;
import com.google.gson.Gson;
import org.json.JSONArray;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.mockito.BDDMockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import java.util.ArrayList;
import java.util.List;
import static org.junit.jupiter.api.Assertions.*;

@AutoConfigureMockMvc
@SpringBootTest
public class HashtagControllerTest {

    @Value("${hashtags.default}")
    private int nHashtags;

    @MockBean
    private HashtagService hashtagService;

    @Autowired
    private HashtagController hashtagController;

    @Autowired
    private MockMvc mockMvc;

    private static final String URL = "http://localhost:8080/hashtags";

    @Before
    public void setUp() {
        hashtagController = new HashtagControllerImpl(hashtagService);
        mockMvc = MockMvcBuilders.standaloneSetup(hashtagController).build();
    }

    @Test
    void getHashtagsClassification() throws Exception {
        List<Hashtag> hashtagList = new ArrayList<>();
        hashtagList.add(new Hashtag("Prueba1", 5));
        hashtagList.add(new Hashtag("Prueba2", 3));
        hashtagList.add(new Hashtag("Prueba3", 2));
        BDDMockito.given(hashtagService.getHashtagsClassification(nHashtags)).willReturn(hashtagList);
        Gson gson = new Gson();
        JSONArray myjson = new JSONArray(gson.toJson(hashtagList));
        RequestBuilder request = MockMvcRequestBuilders.get(URL+"/classification").contentType(MediaType.APPLICATION_JSON).content(String.valueOf(myjson));
        MvcResult result = mockMvc.perform(request).andReturn();
        assertTrue(result.getResponse().getContentAsString().contains("\"hashtags\":\"Prueba1\",\"cont\":5}"));
    }

}
