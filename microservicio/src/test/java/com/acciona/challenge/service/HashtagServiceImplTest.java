package com.acciona.challenge.service;

import com.acciona.challenge.domain.entity.Hashtag;
import com.acciona.challenge.repository.HashtagRepository;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import static org.junit.jupiter.api.Assertions.*;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@SpringBootTest
public class HashtagServiceImplTest {

    @Autowired
    private HashtagService hashtagService;

    @MockBean
    private HashtagRepository repository;

    @Test
    void getHashtagsClassification() {
        List<Hashtag> hashtagList = new ArrayList<>();
        hashtagList.add(new Hashtag("Prueba1", 5));
        hashtagList.add(new Hashtag("Prueba2", 3));
        hashtagList.add(new Hashtag("Prueba3", 2));
        Mockito.when(repository.findAllByOrderByContDesc().stream().limit(3).collect(Collectors.toList())).thenReturn(hashtagList);
        assertEquals(hashtagList, hashtagService.getHashtagsClassification(3));
    }
}
