package com.acciona.challenge.service;

import com.acciona.challenge.domain.entity.Tweet;
import com.acciona.challenge.repository.TweetRepository;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import static org.junit.jupiter.api.Assertions.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@SpringBootTest
public class TweetServiceImplTest {

    @Autowired
    private TweetService tweetService;

    @MockBean
    private TweetRepository repository;

    @Test
    void getAllTweets() {
        List<Tweet> tweets = new ArrayList<>();
        tweets.add(new Tweet("6110d76231a4382f03200881","Prueba","Prueba","Prueba",false,null));
        Mockito.when(repository.findAll()).thenReturn(tweets);
        assertEquals(tweets, tweetService.getAllTweets());
    }

    @Test
    void getValidatedTweets() {
        List<Tweet> tweets = new ArrayList<>();
        tweets.add(new Tweet("6110d76231a4382f03200881","Prueba","Prueba","Prueba",true,null));
        Mockito.when(repository.findAllByUserAndValidation("Prueba", true)).thenReturn(tweets);
        assertEquals(tweets, tweetService.getValidatedTweets("Prueba"));
    }

    @Test
    void validateTweet() {
        Tweet tweet = new Tweet("6110d76231a4382f03200881","Prueba","Prueba","Prueba",false,null);
        Mockito.when(repository.existsById(tweet.get_id())).thenReturn(true);
        Mockito.when(repository.findById(tweet.get_id())).thenReturn(Optional.of(tweet));
        assertTrue(tweetService.validateTweet("6110d76231a4382f03200881"));
    }

    @Test
    void noValidateTweet() {
        Tweet tweet = new Tweet("6110d76231a4382f03200881","Prueba","Prueba","Prueba",false,null);
        Mockito.when(repository.existsById(tweet.get_id())).thenReturn(false);
        assertFalse(tweetService.validateTweet("6110d76231a4382f03200881"));
    }
}
