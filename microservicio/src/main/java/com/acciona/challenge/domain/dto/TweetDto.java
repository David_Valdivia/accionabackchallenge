package com.acciona.challenge.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TweetDto {

    private String _id;
    private String user;
    private String text;
    private String location;
    private boolean validation;

}
