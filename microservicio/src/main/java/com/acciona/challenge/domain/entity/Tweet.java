package com.acciona.challenge.domain.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import twitter4j.HashtagEntity;
import twitter4j.Status;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Document(collection="tweets")
public class Tweet {

    @Id
    private String _id;

    private String user;
    private String text;
    private String location;
    private boolean validation;
    private List<String> hashtags;

    public static Tweet fromStatus(Status status) {
        return Tweet.builder()
                ._id(Long.toString(status.getId()))
                .user(status.getUser().getScreenName())
                .text(status.getText())
                .location(status.getUser().getLocation())
                .validation(false)
                .hashtags(Arrays.stream(status.getHashtagEntities()).map(HashtagEntity::getText).collect(Collectors.toList()))
                .build();
    }

}
