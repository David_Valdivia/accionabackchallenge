package com.acciona.challenge.domain.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;
import javax.persistence.Id;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Document(collection="hashtags")
public class Hashtag {

    @Id
    private String _id;

    private String hashtags;
    private long cont;

    public Hashtag(String hashtags, int cont) {
        this.hashtags = hashtags;
        this.cont = cont;
    }

}
