package com.acciona.challenge.repository;

import com.acciona.challenge.domain.entity.Hashtag;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface HashtagRepository extends MongoRepository<Hashtag, String> {
    boolean existsByHashtags(String hashtag);
    Hashtag findByHashtags(String hashtag);
    List<Hashtag> findAllByOrderByContDesc();
}
