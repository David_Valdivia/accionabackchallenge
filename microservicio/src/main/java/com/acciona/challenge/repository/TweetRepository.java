package com.acciona.challenge.repository;

import com.acciona.challenge.domain.entity.Tweet;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface TweetRepository extends MongoRepository<Tweet, String> {
    List<Tweet> findAllByUserAndValidation(String user, boolean validation);
}
