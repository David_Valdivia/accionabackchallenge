package com.acciona.challenge.repository;

import com.acciona.challenge.domain.entity.Tweet;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ConsumerRepository extends ReactiveMongoRepository<Tweet, String> {
}
