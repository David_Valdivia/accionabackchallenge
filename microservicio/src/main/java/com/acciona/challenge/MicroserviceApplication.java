package com.acciona.challenge;

import com.acciona.challenge.utils.TwitterStream;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.data.mongodb.repository.config.EnableReactiveMongoRepositories;

@SpringBootApplication
@EnableReactiveMongoRepositories
@EnableMongoRepositories
public class MicroserviceApplication {
   public static void main(String[] args) {
       ConfigurableApplicationContext cac = SpringApplication.run(MicroserviceApplication.class, args);

       TwitterStream streamer = (TwitterStream) cac.getBean("twitterStream");
       streamer.startStream();
   }
}
