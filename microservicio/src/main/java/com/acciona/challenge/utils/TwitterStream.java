package com.acciona.challenge.utils;

import com.acciona.challenge.domain.entity.Hashtag;
import com.acciona.challenge.domain.entity.Tweet;
import com.acciona.challenge.repository.ConsumerRepository;
import com.acciona.challenge.repository.HashtagRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;
import twitter4j.*;
import twitter4j.conf.ConfigurationBuilder;
import java.util.List;
import java.util.stream.Collectors;

@Component
@PropertySource("classpath:twitter4j.properties")
public class TwitterStream {

    private final ValidateTweet validateTweet;
    private final ConsumerRepository consumerRepository;
    private final HashtagRepository hashtagRepository;

    @Value("${oauth.consumer-key}")
    private String consumerKey;

    @Value("${oauth.consumer-secret}")
    private String consumerSecret;

    @Value("${oauth.access-token}")
    private String accessToken;

    @Value("${oauth.access-token-secret}")
    private String accessTokenSecret;

    @Autowired
    public TwitterStream(ValidateTweet validateTweet, ConsumerRepository consumerRepository, HashtagRepository hashtagRepository) {
        this.validateTweet = validateTweet;
        this.consumerRepository = consumerRepository;
        this.hashtagRepository = hashtagRepository;
    }

    public void startStream() {
        ConfigurationBuilder configurationBuilder = new ConfigurationBuilder();
        configurationBuilder.setDebugEnabled(true)
                .setOAuthConsumerKey(consumerKey)
                .setOAuthConsumerSecret(consumerSecret)
                .setOAuthAccessToken(accessToken)
                .setOAuthAccessTokenSecret(accessTokenSecret);
        twitter4j.TwitterStream twitterStream = new TwitterStreamFactory(configurationBuilder.build()).getInstance();

        Flux<Status> tweets = Flux.create(sink -> {
            twitterStream.onStatus(sink::next);
            twitterStream.onException(sink::error);
            sink.onCancel(twitterStream::shutdown);
            twitterStream.sample();
        });

        tweets.filter(validateTweet::validateTweet)
                .map(Tweet::fromStatus)
                .flatMap(tweet -> {
                    saveHashtags(tweet);
                    return consumerRepository.save(tweet);
                })
                .subscribe();
    }

    private void saveHashtags(Tweet tweet) {
        List<Hashtag> hashtagList = tweet.getHashtags().stream()
                .map(hashtag -> new Hashtag(hashtag, 0))
                .collect(Collectors.toList());

        hashtagList.forEach(h -> {
            if(hashtagRepository.existsByHashtags(h.getHashtags())) {
                Hashtag hashtag = hashtagRepository.findByHashtags(h.getHashtags());
                hashtag.setCont(hashtag.getCont()+1);
                hashtagRepository.save(hashtag);
            }else {
                hashtagRepository.save(new Hashtag(null, h.getHashtags(), 1));
            }
        });
    }
}
