package com.acciona.challenge.utils;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import twitter4j.Status;
import java.util.List;

@Component
public class ValidateTweet {

    @Value("${list.languajes}")
    private List<String> languajes;

    @Value("${followers.default}")
    private int nFollowers;

    public boolean validateTweet(Status status) {
        return status.getUser().getFollowersCount() > nFollowers && languajes.contains(status.getLang());
    }
}
