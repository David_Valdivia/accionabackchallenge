package com.acciona.challenge.controller;

import com.acciona.challenge.domain.dto.HashtagDto;
import org.springframework.http.ResponseEntity;
import java.util.List;

public interface HashtagController {
    ResponseEntity<List<HashtagDto>> getHashtagsClassification();
}
