package com.acciona.challenge.controller;

import com.acciona.challenge.domain.dto.TweetDto;
import com.acciona.challenge.domain.entity.Tweet;
import com.acciona.challenge.service.TweetService;
import io.swagger.annotations.ApiOperation;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/tweets")
public class TweetControllerImpl implements TweetController {

    private final TweetService tweetService;

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    public TweetControllerImpl(TweetService tweetService) {
        this.tweetService = tweetService;
    }

    @GetMapping("")
    @ApiOperation(value = "Consultar los tweets", notes = "Se consultan todos los tweets almacenados en la base de datos.")
    public ResponseEntity<List<TweetDto>> getAllTweets() {
        return ResponseEntity.ok(tweetService.getAllTweets().stream().map(this::convertToDto).collect(Collectors.toList()));
    }

    @PutMapping("/{id}/validate")
    @ApiOperation(value = "Validar los tweets", notes = "Marcar un tweet como validado introduciendo por parámetro su id.")
    public ResponseEntity<Boolean> validateTweet(@PathVariable("id") final String id) {
        return ResponseEntity.ok(tweetService.validateTweet(id));
    }

    @GetMapping("/users/{user}/validated-tweets")
    @ApiOperation(value = "Consultar los tweets validados", notes = "Se consultan los tweets validados por un usuario introduciendo por parámetro el nombre de usuario.")
    public ResponseEntity<List<TweetDto>> getValidatedTweets(@PathVariable("user") final String user) {
        return ResponseEntity.ok(tweetService.getValidatedTweets(user).stream().map(this::convertToDto).collect(Collectors.toList()));
    }

    private TweetDto convertToDto(Tweet tweet) {
        return modelMapper.map(tweet, TweetDto.class);
    }

}
