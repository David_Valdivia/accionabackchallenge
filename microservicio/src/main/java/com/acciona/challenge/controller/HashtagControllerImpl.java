package com.acciona.challenge.controller;

import com.acciona.challenge.domain.dto.HashtagDto;
import com.acciona.challenge.domain.entity.Hashtag;
import com.acciona.challenge.service.HashtagService;
import io.swagger.annotations.ApiOperation;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/hashtags")
public class HashtagControllerImpl implements HashtagController {

    private final HashtagService hashtagService;

    @Value("${hashtags.default}")
    private int nHashtags;

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    public HashtagControllerImpl(HashtagService hashtagService) {
        this.hashtagService = hashtagService;
    }

    @GetMapping("/classification")
    @ApiOperation(value = "Consultar una clasificacion de hashtags", notes = "Se consulta la clasificacion de los N hashtags mas usados.")
    public ResponseEntity<List<HashtagDto>> getHashtagsClassification() {
        return ResponseEntity.ok(hashtagService.getHashtagsClassification(nHashtags).stream().map(this::convertToDto).collect(Collectors.toList()));
    }

    private HashtagDto convertToDto(Hashtag hashtag) {
        return modelMapper.map(hashtag, HashtagDto.class);
    }
}
