package com.acciona.challenge.controller;

import com.acciona.challenge.domain.dto.TweetDto;
import org.springframework.http.ResponseEntity;
import java.util.List;

public interface TweetController {
    ResponseEntity<List<TweetDto>> getAllTweets();
    ResponseEntity<Boolean> validateTweet(String id);
    ResponseEntity<List<TweetDto>> getValidatedTweets(String user);
}
