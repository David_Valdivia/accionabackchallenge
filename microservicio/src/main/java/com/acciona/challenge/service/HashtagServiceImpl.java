package com.acciona.challenge.service;

import com.acciona.challenge.domain.entity.Hashtag;
import com.acciona.challenge.repository.HashtagRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class HashtagServiceImpl implements HashtagService {

    private final HashtagRepository hashtagRepository;

    @Autowired
    public HashtagServiceImpl(HashtagRepository hashtagRepository) {
        this.hashtagRepository = hashtagRepository;
    }

    @Override
    public List<Hashtag> getHashtagsClassification(int n) {
        return hashtagRepository.findAllByOrderByContDesc().stream().limit(n).collect(Collectors.toList());
    }
}
