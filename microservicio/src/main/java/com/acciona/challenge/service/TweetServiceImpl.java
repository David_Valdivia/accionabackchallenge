package com.acciona.challenge.service;

import com.acciona.challenge.domain.entity.Tweet;
import com.acciona.challenge.repository.TweetRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.*;

@Service
public class TweetServiceImpl implements TweetService {

    private final TweetRepository repository;

    @Autowired
    public TweetServiceImpl(TweetRepository repository) {
        this.repository = repository;
    }

    @Override
    public List<Tweet> getAllTweets() {
        return repository.findAll().isEmpty() ? null : repository.findAll();
    }

    @Override
    public boolean validateTweet(String id) {
        if (!repository.existsById(id)) return false;
        Tweet tweet = repository.findById(id).get();
        tweet.setValidation(true);
        repository.save(tweet);
        return true;
    }

    @Override
    public List<Tweet> getValidatedTweets(String user) {
        return repository.findAllByUserAndValidation(user, true);
    }

}
