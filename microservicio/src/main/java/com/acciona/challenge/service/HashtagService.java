package com.acciona.challenge.service;

import com.acciona.challenge.domain.entity.Hashtag;
import java.util.List;

public interface HashtagService {
    List<Hashtag> getHashtagsClassification(int n);
}
