package com.acciona.challenge.service;

import com.acciona.challenge.domain.entity.Tweet;
import java.util.List;

public interface TweetService {
    List<Tweet> getAllTweets();
    boolean validateTweet(String id);
    List<Tweet> getValidatedTweets(String user);
}
