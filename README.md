# Developers Challenge Back (Acciona)

Se implementa un microservicio que consume tweets y, basado en unos criterios de configuración, los persiste en una base de datos para su gestión a través de una API REST.


## Ejecución 🚀
1. Descargar el proyecto.

2. Introducir dentro del archivo **_twitter4j.properties_** las claves y los tokens para poder acceder a la API de Twitter.

    ![API_Twitter](/uploads/0b9e033b3d56e73d521601936f52c73d/API_Twitter.PNG)

3. Instalar [MongoDB](https://www.mongodb.com/try/download/community).

4. Instalar [Maven](https://wiki.uqbar.org/wiki/articles/guia-de-instalacion-de-maven.html).

5. Dentro del directorio **_microservicio_**, abrir el terminal y ejecutar el comando `mvn spring-boot:run`

    ![Ejecucion](/uploads/1ca0d14f1a2fc0c982736503c035aa17/Ejecucion.PNG)


## API REST 📦

- Consultar los tweets: [http://localhost:8080/tweets](http://localhost:8080/tweets)

- Marcar un tweet como validado: [http://localhost:8080/tweets/{idTweet}/validate](http://localhost:8080/tweets/{idTweet}/validate)
- Consultar los tweets validados por usuario: [http://localhost:8080/tweets/users/{usuario}/validated-tweets](http://localhost:8080/tweets/users/{usuario}/validated-tweets)
- Consultar la clasificación de hashtags más usados: [http://localhost:8080/hashtags/classification](http://localhost:8080/hashtags/classification)

## Notas 📖

La API se encuentra documentada con la herramienta Swagger, puede acceder a la documentación a 
través del siguiente enlace: [http://localhost:8080/swagger-ui/index.html#/](http://localhost:8080/swagger-ui/index.html#/) 

El servicio está disponible en el puerto 8080.

Se pueden cambiar los valores por defecto (lista de idiomas permitidos, número de seguidores, etc) 
dentro del archivo **_application.yml_**.

![Valores_por_defecto](/uploads/645529ce70322b74e27cdeb22a165e29/Valores_por_defecto.PNG)

La base de datos utilizada ha sido MongoDB.

También puede ejecutar el servicio mediante el archivo **_MicroserviceApplication_**.
